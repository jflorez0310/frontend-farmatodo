export class Comic {
  id: string;
  thumbnail: Object;
  price: number;
  title: string;
  description: string;
}
