export class Character {
  id: string;
  name: string;
  description: string;
  modified: string;
  thumbail: any;
  resourceURI: any;
  comics: any;
  series: any;
  stories: any;
  events: any;
  urls: any;
}
