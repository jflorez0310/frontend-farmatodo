import { Component, OnInit } from '@angular/core';
import { SharingDataService} from '../../services/sharing-data.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {


  private name: string;

  constructor(private sharingDataService: SharingDataService) { }

  ngOnInit() {

  }

  private buscar() {
    this.sharingDataService.setData(this.name);
  }

}
