import { Component, OnInit,  TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { CharactersService } from '../../services/characters.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { SharingDataService } from '../../services/sharing-data.service';
import { ModalComponent } from '../modal/modal.component';


@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  bsModalRef: BsModalRef;
  dataSubscription: Subscription;
  name: string;


  data: Observable<any>;
  results: Observable<any[]>;

  constructor(private characterService: CharactersService, public sharingDataService: SharingDataService, private modalService: BsModalService) {

  }

  ngOnInit() {
    this.getData(null);
    this.dataSubscription = this.sharingDataService.dataSource$
      .subscribe(data => {
        this.name = data;
        console.log(this.name);
        this.getData(this.name);
      });
  }

  openModalWithComponent(url) {
    console.log('url', url);
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Modal with component'
    };
    this.bsModalRef = this.modalService.show(ModalComponent, {initialState});
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  getData(name: string) {
    if (name !== null && name !== undefined) {
      this.characterService.findByName(name)
        .subscribe(data => {
          this.data = data.data;
          this.results = data.data.results;
          console.log('result', this.results);
        });
    } else {
      this.characterService.findAll()
        .subscribe(data => {
          this.data = data.data;
          this.results = data.data.results;
          console.log('result', this.results);
        });
    }
  }

}
