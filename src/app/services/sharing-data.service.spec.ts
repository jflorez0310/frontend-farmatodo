import { TestBed, inject } from '@angular/core/testing';

import { SharingDataServiceService } from './sharing-data.service';

describe('SharingDataServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SharingDataServiceService]
    });
  });

  it('should be created', inject([SharingDataServiceService], (service: SharingDataServiceService) => {
    expect(service).toBeTruthy();
  }));
});
