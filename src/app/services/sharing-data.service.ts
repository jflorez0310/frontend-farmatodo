import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class SharingDataService {

  data: string;
  public _dataSource = new BehaviorSubject<string>('');
  dataSource$ = this._dataSource.asObservable();


  constructor() { }

  // Metodo que envia los datos a los componentes subscritos
  public setData(data: string) {
    this.data = data;
    this._dataSource.next(data);
  }
}
