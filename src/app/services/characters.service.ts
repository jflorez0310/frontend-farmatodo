import { Injectable } from '@angular/core';
import { HttpClient, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class CharactersService {

  domain: string = "http://gateway.marvel.com/v1/public/characters?ts=1522342488&apikey=5e1a1ac97631eeac4253875bbf6a1906&hash=1e7e56f36f436ba0f173f381af62f131";

  constructor(private http:HttpClient) {

  }


  findByName(name: string): Observable<any> {
    let httpParams = new HttpParams()
                      .set('limit', '10')
                      .set('nameStartsWith', name);
    return this.http.get(this.domain, {params: httpParams});
  }


  findAll(): Observable<any> {
    let httpParams = new HttpParams()
      .set('limit', '10');
    return this.http.get(this.domain, {params: httpParams});
  }


}
